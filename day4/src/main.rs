fn get_input() -> (u32, u32) {
    let mut inputs = INPUT.split('-')
        .map(str::parse::<u32>)
        .map(Result::unwrap);
    (inputs.next().unwrap(), inputs.next().unwrap())
}

fn criteria(mut x: u32) -> bool {
    let mut has_double = false;
    let mut last = x % 10;
    x /= 10;
    while x > 0 {
        let curr = x % 10;
        if curr == last {
            has_double = true;
        } else if curr > last {
            return false;
        }
        last = curr;
        x /= 10;
    }
    has_double
}

fn criteria_two(mut x: u32) -> bool {
    let mut last = x % 10;
    let mut repeating_digit = last;
    let mut repeating_count = 1;
    let mut has_double = false;
    x /= 10;
    while x > 0 {
        let curr = x % 10;
        if curr == last {
            repeating_count += 1;
        } else if curr < last {
            if repeating_count == 2 {
                has_double = true;
            }
            repeating_digit = curr;
            repeating_count = 1;
        } else {
            return false;
        }

        last = curr;
        x /= 10;
    }
    if repeating_count == 2 {
        has_double = true;
    }
    has_double
}

fn main() {
    let (from, to) = get_input();
    let mut n = 0;
    for i in from..=to {
        if criteria_two(i) {
            n += 1;
        }
    }
    println!("{}", n);
}

const INPUT: &str = "172851-675869";
