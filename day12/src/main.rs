#[derive(Copy, Clone, Hash, Eq, PartialEq)]
struct Moon {
    x: i32,
    y: i32,
    z: i32,
    vx: i32,
    vy: i32,
    vz: i32,
}

impl Moon {
    fn new(x: i32, y: i32, z: i32) -> Self {
        Self { x, y, z, vx: 0, vy: 0, vz: 0 }
    }

    fn energy(&self) -> u64 {
        let pot = self.x.abs() + self.y.abs() + self.z.abs();
        let kin = self.vx.abs() + self.vy.abs() + self.vz.abs();
        pot as u64 * kin as u64
    }
}


#[derive(Copy, Clone, Hash, Eq, PartialEq)]
struct State {
    moons: [Moon; 4]
}

impl State {
    fn gravity(&mut self) {
        for x in 0..self.moons.len() {
            for y in x + 1..self.moons.len() {
                let dx = (self.moons[y].x - self.moons[x].x).signum();
                let dy = (self.moons[y].y - self.moons[x].y).signum();
                let dz = (self.moons[y].z - self.moons[x].z).signum();

                let x = &mut self.moons[x];
                x.vx += dx;
                x.vy += dy;
                x.vz += dz;

                let y = &mut self.moons[y];
                y.vx -= dx;
                y.vy -= dy;
                y.vz -= dz;
            }
        }
    }

    fn velocity(&mut self) {
        for moon in self.moons.iter_mut() {
            moon.x += moon.vx;
            moon.y += moon.vy;
            moon.z += moon.vz;
        }
    }

    #[allow(unused)]
    fn energy(&self) -> u64 {
        self.moons.iter()
            .map(Moon::energy)
            .sum()
    }

    #[allow(unused)]
    fn display(&self, idx: usize) {
        let mut min_x = 0;
        let mut min_y = 0;
        let mut max_x = 0;
        let mut max_y = 0;
        for moon in self.moons.iter() {
            min_x = min_x.min(moon.x);
            min_y = min_y.min(moon.y);
            max_x = max_x.max(moon.x);
            max_y = max_y.max(moon.y);
        }

        const SIZE: u32 = 100;
        let mut image = image::DynamicImage::new_rgb8(SIZE, SIZE);
        for moon in self.moons.iter() {
            use image::GenericImage;
            let x = ((SIZE / 2) as i32 + moon.x) as u32;
            let y = ((SIZE / 2) as i32 + moon.y) as u32;
            image.put_pixel(x, y, image::Rgba([255, 255, 255, 255]));
        }
        let _ = image.save(format!("./day12/anim/img{}.png", idx));
        //let _ = std::io::stdin().read_line(&mut String::new());
    }
}

fn gcd(a: u64, b: u64) -> u64 {
    if a == 0 {
        b
    } else if b == 0 {
        a
    } else {
        let min = a.min(b);
        let max = a.max(b);
        gcd(min, max % min)
    }
}

fn gcm(a: u64, b: u64) -> u64 {
    a * b / gcd(a, b)
}

fn sim(moons: &mut [(i32, i32)]) -> u64 {
    let mut iter = 0;
    let mut prev = Vec::<Vec<(i32, i32)>>::new();
    loop {
        for x in 0..moons.len() {
            for y in x + 1..moons.len() {
                let d = (moons[y].0 - moons[x].0).signum();
                moons[x].1 += d;
                moons[y].1 -= d;
            }
        }
        for (p, v) in moons.iter_mut() {
            *p += *v;
        }
        for prev in &prev {
            let mut same = true;
            for i in 0..moons.len() {
                if moons[i] != prev[i] {
                    same = false;
                }
            }
            if same { return iter; }
        }
        prev.push(moons.to_vec());
        iter += 1;
    }
    iter
}

fn main() {
    let x = std::thread::spawn(|| sim(&mut [(17, 0), (2, 0), (-1, 0), (12, 0)]));
    let y = std::thread::spawn(|| sim(&mut [(-12, 0), (1, 0), (-17, 0), (-14, 0)]));
    let z = std::thread::spawn(|| sim(&mut [(13, 0), (1, 0), (7, 0), (18, 0)]));

    let x = x.join().unwrap();
    println!("X: {}", x);

    let y = y.join().unwrap();
    println!("Y: {}", y);

    let z = z.join().unwrap();
    println!("Z: {}", z);

    let period = gcm(gcm(x, y), z);
    println!("{}", period);
}

fn old() {
    let mut state = State {
        moons: [
            /*Moon::new(-1, 0, 2),
            Moon::new(2, -10, -7),
            Moon::new(4, -8, 8),
            Moon::new(3, 5, -1),*/

            /*Moon::new(-8, -10, 0),
            Moon::new(5, 5, 10),
            Moon::new(2, -7, 3),
            Moon::new(9, -8, -3),*/

            Moon::new(17, -12, 13),
            Moon::new(2, 1, 1),
            Moon::new(-1, -17, 7),
            Moon::new(12, -14, 18),
        ]
    };
    let mut previous = Vec::<State>::with_capacity(100_000_000);
    let mut iter = 0;
    let mut period_x = None;
    let mut period_y = None;
    let mut period_z = None;
    loop {
        state.gravity();
        state.velocity();

        for previous in &previous {
            let mut px = true;
            let mut py = true;
            let mut pz = true;
            for i in 0..state.moons.len() {
                let this = &state.moons[i];
                let prev = &previous.moons[i];
                if period_x.is_some() || this.x != prev.x || this.vx != prev.vx {
                    px = false;
                }
                if period_y.is_some() || this.y != prev.y || this.vy != prev.vy {
                    py = false;
                }
                if period_z.is_some() || this.z != prev.z || this.vz != prev.vz {
                    pz = false;
                }
            }
            if px {
                println!("X: {}", iter);
                period_x = Some(iter);
            }
            if py {
                println!("Y: {}", iter);
                period_y = Some(iter);
            }
            if pz {
                println!("Z: {}", iter);
                period_z = Some(iter);
            }
        }

        if period_x.is_some() && period_y.is_some() && period_z.is_some() {
            break;
        }

        previous.push(state);

        iter += 1;
    }

    let px = period_x.unwrap();
    let py = period_y.unwrap();
    let pz = period_z.unwrap();
    let period = gcm(gcm(px, py), pz);
    println!("{} {} {}", px, py, pz);
    println!("{}", period);
}
