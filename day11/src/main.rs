use std::collections::{VecDeque, HashMap};


type DataType = i64;

enum Mode {
    Position,
    Immediate,
    Relative,
}

impl Mode {
    fn from_data(code: DataType) -> Self {
        match code {
            0 => Mode::Position,
            1 => Mode::Immediate,
            2 => Mode::Relative,
            x => panic!("Invalid mode{}!", x),
        }
    }
}

enum Opcode {
    Add,
    Mul,
    In,
    Out,
    Jit,
    Jif,
    Lt,
    Eq,
    SetRel,
    Halt,
}

struct Instr {
    opcode: Opcode,
    mode_1: Mode,
    mode_2: Mode,
    mode_3: Mode,
}

impl Instr {
    fn from_data(val: DataType, pc: usize) -> Instr {
        let opcode = match val % 100 {
            1 => Opcode::Add,
            2 => Opcode::Mul,
            3 => Opcode::In,
            4 => Opcode::Out,
            5 => Opcode::Jit,
            6 => Opcode::Jif,
            7 => Opcode::Lt,
            8 => Opcode::Eq,
            9 => Opcode::SetRel,
            99 => Opcode::Halt,
            opc => panic!("Invalid opcode {} at address {}!", opc, pc),
        };
        let mode_1 = Mode::from_data(val / 100 % 10);
        let mode_2 = Mode::from_data(val / 1000 % 10);
        let mode_3 = Mode::from_data(val / 10000 % 10);
        Self { opcode, mode_1, mode_2, mode_3 }
    }
}

fn get_program() -> Vec<DataType> {
    INPUT.split(',')
        .map(str::trim)
        .map(str::parse::<DataType>)
        .filter_map(Result::ok)
        .collect()
}

#[derive(Clone)]
struct Machine {
    pc: usize,
    rel_base: isize,
    state: Vec<DataType>,
    input: VecDeque<DataType>,
    output: VecDeque<DataType>,
    extended: HashMap<usize, DataType>,
    halted: bool,
}

impl Machine {
    fn new(program: Vec<DataType>) -> Self {
        Self {
            pc: 0,
            rel_base: 0,
            state: program,
            input: VecDeque::new(),
            output: VecDeque::new(),
            extended: HashMap::new(),
            halted: false,
        }
    }

    fn load(&self, addr: usize) -> DataType {
        if addr < self.state.len() {
            self.state[addr]
        } else {
            self.extended.get(&addr).cloned().unwrap_or_default()
        }
    }

    fn store(&mut self, addr: usize, val: DataType) {
        if addr < self.state.len() {
            self.state[addr] = val;
        } else {
            *self.extended.entry(addr).or_default() = val;
        }
    }

    fn read(&self, mode: Mode, addr: usize) -> DataType {
        let addr = self.load(addr);
        match mode {
            Mode::Position => self.load(addr as usize),
            Mode::Immediate => addr,
            Mode::Relative => self.load((self.rel_base + addr as isize) as usize),
        }
    }

    fn write(&mut self, mode: Mode, addr: usize, val: DataType) {
        let addr = self.load(addr);
        match mode {
            Mode::Position => self.store(addr as usize, val),
            Mode::Immediate => panic!("Immediate mode can not be used for write parameters!"),
            Mode::Relative => self.store((self.rel_base + addr as isize) as usize, val),
        }
    }

    fn exec(&mut self, instr: Instr) -> bool {
        match instr.opcode {
            Opcode::Add => {
                let a = self.read(instr.mode_1, self.pc + 1);
                let b = self.read(instr.mode_2, self.pc + 2);
                self.write(instr.mode_3, self.pc + 3, a + b);
                self.pc += 4;
            }
            Opcode::Mul => {
                let a = self.read(instr.mode_1, self.pc + 1);
                let b = self.read(instr.mode_2, self.pc + 2);
                self.write(instr.mode_3, self.pc + 3, a * b);
                self.pc += 4;
            }
            Opcode::In => {
                let val = self.input.pop_front().expect("No inputs left!");
                self.write(instr.mode_1, self.pc + 1, val);
                self.pc += 2;
            }
            Opcode::Out => {
                let val = self.read(instr.mode_1, self.pc + 1);
                self.output.push_back(val);
                self.pc += 2;
            }
            Opcode::Jit => {
                let x = self.read(instr.mode_1, self.pc + 1);
                if x != 0 {
                    self.pc = self.read(instr.mode_2, self.pc + 2) as usize;
                } else {
                    self.pc += 3;
                }
            }
            Opcode::Jif => {
                let x = self.read(instr.mode_1, self.pc + 1);
                if x == 0 {
                    self.pc = self.read(instr.mode_2, self.pc + 2) as usize;
                } else {
                    self.pc += 3;
                }
            }
            Opcode::Lt => {
                let a = self.read(instr.mode_1, self.pc + 1);
                let b = self.read(instr.mode_2, self.pc + 2);
                self.write(instr.mode_3, self.pc + 3, if a < b { 1 } else { 0 });
                self.pc += 4;
            }
            Opcode::Eq => {
                let a = self.read(instr.mode_1, self.pc + 1);
                let b = self.read(instr.mode_2, self.pc + 2);
                self.write(instr.mode_3, self.pc + 3, if a == b { 1 } else { 0 });
                self.pc += 4;
            }
            Opcode::SetRel => {
                self.rel_base += self.read(instr.mode_1, self.pc + 1) as isize;
                self.pc += 2;
            }
            Opcode::Halt => return false,
        }
        true
    }

    fn run(&mut self) -> Option<DataType> {
        while self.exec(Instr::from_data(self.load(self.pc), self.pc)) {
            if let output @ Some(_) = self.output.pop_front() {
                return output;
            }
        }
        self.halted = true;
        None
    }
}

fn main() {
    let mut vm = Machine::new(get_program());
    let mut x = 0;
    let mut y = 0;
    let mut d = 0;
    let mut painted = HashMap::<(i32, i32), bool>::new();

    loop {
        let input = *painted.entry((x, y)).or_insert(true);
        vm.input.push_back(if input { 1 } else { 0 });

        if let Some(result) = vm.run() {
            painted.insert((x, y), if result == 0 { false } else { true });
        }
        if let Some(result) = vm.run() {
            d = (d + if result == 0 { 3 } else { 1 }) % 4;
        }

        let (dx, dy) = match d {
            0 => (0, -1),
            1 => (1, 0),
            2 => (0, 1),
            3 => (-1, 0),
            _ => panic!("Invalid direction!"),
        };
        x += dx;
        y += dy;

        if vm.halted {
            break;
        }
    }

    println!("{}", painted.len());

    let mut min_x = 0;
    let mut max_x = 0;
    let mut min_y = 0;
    let mut max_y = 0;
    for ((x, y), _) in &painted {
        min_x = min_x.min(*x);
        max_x = max_x.max(*x);
        min_y = min_y.min(*y);
        max_y = max_y.max(*y);
    }

    let width = (max_x - min_x + 1) as u32;
    let height = (max_y - min_y + 1) as u32;
    let mut image = image::DynamicImage::new_rgb8(width, height);
    for ((x, y), c) in painted {
        use image::GenericImage;
        let x = (x - min_x) as u32;
        let y = (y - min_y) as u32;
        let c = if c {
            image::Rgba([0, 0, 0, 255])
        } else {
            image::Rgba([255, 255, 255, 255])
        };
        image.put_pixel(x, y, c);
    }
    let _ = image.save("./day11/img.png");
}

/*
    1 => Opcode::Add,
    2 => Opcode::Mul,
    3 => Opcode::In,
    4 => Opcode::Out,
    5 => Opcode::Jit,
    6 => Opcode::Jif,
    7 => Opcode::Lt,
    8 => Opcode::Eq,
    9 => Opcode::SetRel,
    99 => Opcode::Halt,
*/

const INPUT: &str = "3,8,1005,8,318,1106,0,11,0,0,0,104,1,104,0,3,8,102,-1,8,10,1001,10,1,10,4,10,1008,8,1,10,4,10,101,0,8,29,1,107,12,10,2,1003,8,10,3,8,102,-1,8,10,1001,10,1,10,4,10,1008,8,0,10,4,10,1002,8,1,59,1,108,18,10,2,6,7,10,2,1006,3,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,1,10,4,10,1002,8,1,93,1,1102,11,10,3,8,102,-1,8,10,1001,10,1,10,4,10,108,1,8,10,4,10,101,0,8,118,2,1102,10,10,3,8,102,-1,8,10,101,1,10,10,4,10,1008,8,0,10,4,10,101,0,8,145,1006,0,17,1006,0,67,3,8,1002,8,-1,10,101,1,10,10,4,10,1008,8,0,10,4,10,101,0,8,173,2,1109,4,10,1006,0,20,3,8,102,-1,8,10,1001,10,1,10,4,10,108,0,8,10,4,10,102,1,8,201,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,0,10,4,10,1002,8,1,224,1006,0,6,1,1008,17,10,2,101,5,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,108,1,8,10,4,10,1001,8,0,256,2,1107,7,10,1,2,4,10,2,2,12,10,1006,0,82,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,1,10,4,10,1002,8,1,294,2,1107,2,10,101,1,9,9,1007,9,988,10,1005,10,15,99,109,640,104,0,104,1,21102,1,837548352256,1,21102,335,1,0,1105,1,439,21102,1,47677543180,1,21102,346,1,0,1106,0,439,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,21102,1,235190374592,1,21101,393,0,0,1105,1,439,21102,3451060455,1,1,21102,404,1,0,1105,1,439,3,10,104,0,104,0,3,10,104,0,104,0,21102,837896909668,1,1,21102,1,427,0,1105,1,439,21102,1,709580555020,1,21102,438,1,0,1105,1,439,99,109,2,21201,-1,0,1,21102,1,40,2,21102,1,470,3,21102,460,1,0,1106,0,503,109,-2,2105,1,0,0,1,0,0,1,109,2,3,10,204,-1,1001,465,466,481,4,0,1001,465,1,465,108,4,465,10,1006,10,497,1101,0,0,465,109,-2,2105,1,0,0,109,4,1201,-1,0,502,1207,-3,0,10,1006,10,520,21101,0,0,-3,21202,-3,1,1,22101,0,-2,2,21101,1,0,3,21101,0,539,0,1106,0,544,109,-4,2105,1,0,109,5,1207,-3,1,10,1006,10,567,2207,-4,-2,10,1006,10,567,21202,-4,1,-4,1105,1,635,22101,0,-4,1,21201,-3,-1,2,21202,-2,2,3,21101,0,586,0,1105,1,544,22102,1,1,-4,21102,1,1,-1,2207,-4,-2,10,1006,10,605,21102,1,0,-1,22202,-2,-1,-2,2107,0,-3,10,1006,10,627,21202,-1,1,1,21101,627,0,0,105,1,502,21202,-2,-1,-2,22201,-4,-2,-4,109,-5,2105,1,0";