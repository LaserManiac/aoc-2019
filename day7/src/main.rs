use std::collections::VecDeque;


enum Mode {
    Position,
    Immediate,
}

impl Mode {
    fn load(&self, pc: usize, state: &mut [i32]) -> i32 {
        match self {
            Mode::Position => state[state[pc] as usize],
            Mode::Immediate => state[pc],
        }
    }
}

enum Opcode {
    Add,
    Mul,
    In,
    Out,
    Jit,
    Jif,
    Lt,
    Eq,
    Halt,
}

struct Instr {
    opcode: Opcode,
    mode_1: Mode,
    mode_2: Mode,
    mode_3: Mode,
}

impl Instr {
    fn from_i32(val: i32) -> Instr {
        let opcode = match val % 100 {
            1 => Opcode::Add,
            2 => Opcode::Mul,
            3 => Opcode::In,
            4 => Opcode::Out,
            5 => Opcode::Jit,
            6 => Opcode::Jif,
            7 => Opcode::Lt,
            8 => Opcode::Eq,
            99 => Opcode::Halt,
            opc => panic!("Invalid opcode {}!", opc),
        };
        let mode_1 = if val / 100 % 10 == 0 { Mode::Position } else { Mode::Immediate };
        let mode_2 = if val / 1000 % 10 == 0 { Mode::Position } else { Mode::Immediate };
        let mode_3 = if val / 10000 % 10 == 0 { Mode::Position } else { Mode::Immediate };
        Self { opcode, mode_1, mode_2, mode_3 }
    }

    fn exec(&self,
            pc: usize,
            state: &mut [i32],
            input: &mut VecDeque<i32>,
            output: &mut VecDeque<i32>)
            -> Option<usize> {
        match self.opcode {
            Opcode::Add => {
                let a = self.mode_1.load(pc + 1, state);
                let b = self.mode_2.load(pc + 2, state);
                let addr = state[pc + 3] as usize;
                state[addr] = a + b;
                Some(pc + 4)
            }
            Opcode::Mul => {
                let a = self.mode_1.load(pc + 1, state);
                let b = self.mode_2.load(pc + 2, state);
                let addr = state[pc + 3] as usize;
                state[addr] = a * b;
                Some(pc + 4)
            }
            Opcode::In => {
                let val = input.pop_front().expect("No inputs left!");
                let addr = state[pc + 1] as usize;
                state[addr] = val;
                Some(pc + 2)
            }
            Opcode::Out => {
                let val = self.mode_1.load(pc + 1, state);
                output.push_back(val);
                Some(pc + 2)
            }
            Opcode::Jit => {
                let x = self.mode_1.load(pc + 1, state);
                if x != 0 {
                    let pc = self.mode_2.load(pc + 2, state) as usize;
                    Some(pc)
                } else {
                    Some(pc + 3)
                }
            }
            Opcode::Jif => {
                let x = self.mode_1.load(pc + 1, state);
                if x == 0 {
                    let pc = self.mode_2.load(pc + 2, state) as usize;
                    Some(pc)
                } else {
                    Some(pc + 3)
                }
            }
            Opcode::Lt => {
                let a = self.mode_1.load(pc + 1, state);
                let b = self.mode_2.load(pc + 2, state);
                let addr = state[pc + 3] as usize;
                state[addr] = if a < b { 1 } else { 0 };
                Some(pc + 4)
            }
            Opcode::Eq => {
                let a = self.mode_1.load(pc + 1, state);
                let b = self.mode_2.load(pc + 2, state);
                let addr = state[pc + 3] as usize;
                state[addr] = if a == b { 1 } else { 0 };
                Some(pc + 4)
            }
            Opcode::Halt => None,
        }
    }
}

fn get_program() -> Vec<i32> {
    INPUT.split(',')
        .map(str::trim)
        .map(str::parse::<i32>)
        .filter_map(Result::ok)
        .collect()
}

#[derive(Clone)]
struct Machine {
    pc: usize,
    state: Vec<i32>,
    input: VecDeque<i32>,
    output: VecDeque<i32>,
    halted: bool,
}

impl Machine {
    fn new(program: Vec<i32>) -> Self {
        Self {
            pc: 0,
            state: program,
            input: VecDeque::new(),
            output: VecDeque::new(),
            halted: false,
        }
    }

    fn run(&mut self) -> Option<i32> {
        loop {
            let instr = Instr::from_i32(self.state[self.pc]);
            match instr.exec(self.pc, &mut self.state, &mut self.input, &mut self.output) {
                Some(next_pc) => self.pc = next_pc,
                None => {
                    self.halted = true;
                    break None;
                }
            }
            if let output @ Some(_) = self.output.pop_front() {
                break output;
            }
        }
    }
}

fn part2() {
    let mut max = 0;
    for a in 5..=9 {
        for b in 5..=9 {
            if b == a { continue; }
            for c in 5..=9 {
                if c == b || c == a { continue; }
                for d in 5..=9 {
                    if d == c || d == b || d == a { continue; }
                    for e in 5..=9 {
                        if e == d || e == c || e == b || e == a { continue; }
                        let phases = vec![a, b, c, d, e];
                        let mut machines = vec![Machine::new(get_program()); phases.len()];
                        for (phase, machine) in phases.iter().zip(machines.iter_mut()) {
                            machine.input.push_back(*phase);
                        }

                        let mut idx = 0;
                        let mut val = 0;
                        loop {
                            let machine = &mut machines[idx];
                            if machine.halted {
                                break;
                            }

                            machine.input.push_back(val);
                            if let Some(next) = machine.run() {
                                val = next;
                            }

                            idx = (idx + 1) % phases.len();
                        }

                        max = max.max(val);
                    }
                }
            }
        }
    }
    println!("{}", max);
}

fn main() {
    part2();
}

const INPUT: &str = "3,8,1001,8,10,8,105,1,0,0,21,42,51,60,77,94,175,256,337,418,99999,3,9,1001,9,4,9,102,5,9,9,1001,9,3,9,102,5,9,9,4,9,99,3,9,102,2,9,9,4,9,99,3,9,1001,9,3,9,4,9,99,3,9,101,4,9,9,1002,9,4,9,101,5,9,9,4,9,99,3,9,1002,9,5,9,101,3,9,9,102,2,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,99,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,99";
