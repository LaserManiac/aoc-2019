fn get_input() -> Map {
    let map = INPUT.lines()
        .filter(|l| !l.is_empty())
        .map(|l| l.chars()
            .map(|c| if c == '#' { true } else { false })
            .collect::<Vec<_>>())
        .collect::<Vec<_>>();
    let width = map.len();
    let height = map[0].len();
    let data = map.into_iter()
        .flatten()
        .collect();
    Map { data, width, height }
}

fn smallest_fraction(mut a: isize, mut b: isize) -> (isize, isize) {
    'outer: loop {
        for i in (2..=a.abs().max(b.abs())).into_iter().rev() {
            if (a / i) * i == a && (b / i) * i == b {
                a /= i;
                b /= i;
                continue 'outer;
            }
        }
        break;
    }
    (a, b)
}

struct Map {
    data: Vec<bool>,
    width: usize,
    height: usize,
}

impl Map {
    #[inline]
    fn get(&self, x: usize, y: usize) -> bool {
        self.data[y * self.width + x]
    }

    #[inline]
    fn set(&mut self, x: usize, y: usize, val: bool) {
        self.data[y * self.width + x] = val;
    }

    fn occluded(&self, src_x: isize, src_y: isize, tgt_x: isize, tgt_y: isize) -> bool {
        let fdx = tgt_x - src_x;
        let fdy = tgt_y - src_y;
        let (dx, dy) = smallest_fraction(fdx, fdy);

        let mut x = src_x + dx;
        let mut y = src_y + dy;
        while x != tgt_x || y != tgt_y {
            if self.get(x as _, y as _) {
                return true;
            }
            x += dx;
            y += dy;
        }

        false
    }
}

fn get_angle(dx: i32, dy: i32) -> f32 {
    if dx == 0 {
        if dy < 0 {
            std::f32::consts::FRAC_PI_2
        } else {
            3.0 * std::f32::consts::FRAC_PI_2
        }
    } else {
        let mut angle = f32::atan2(-dy as f32, dx as f32);
        if angle < 0.0 {
            angle += 2.0 * std::f32::consts::PI
        }
        angle
    }
}

fn main() {
    let mut map = get_input();
    let mut max = (0, 0, 0);
    for y in 0..map.height {
        for x in 0..map.width {
            if map.get(x, y) {
                let mut counter = 0;
                for ty in 0..map.height {
                    for tx in 0..map.width {
                        if tx == x && ty == y {
                            continue;
                        } else if map.get(tx, ty)
                            && !map.occluded(x as _, y as _, tx as _, ty as _) {
                            counter += 1;
                        }
                    }
                }
                if counter > max.0 {
                    max = (counter, x, y);
                }
            }
        }
    }
    println!("{} at {}x{}", max.0, max.1, max.2);

    let (_, sx, sy) = max;
    let mut visible = Vec::new();
    let mut destroyed = 0;
    'outer: loop {
        visible.clear();
        for y in 0..map.height {
            for x in 0..map.width {
                if x == sx && y == sy {
                    continue;
                } else if map.get(x, y)
                    && !map.occluded(sx as _, sy as _, x as _, y as _) {
                    visible.push((x, y));
                }
            }
        }
        visible.sort_by_cached_key(|(x, y)| {
            let dx = *x - sx;
            let dy = *y - sy;
            let ang = get_angle(dx as _, dy as _);
            use std::f32::consts::{PI, FRAC_PI_2};
            let mut rot270 = ang + 3.0 * FRAC_PI_2;
            if rot270 > 2.0 * PI {
                rot270 -= 2.0 * PI;
            }
            let rev = 2.0 * PI - rot270;
            (rev * 10000.0) as i64
        });
        for (x, y) in visible.drain(..) {
            destroyed += 1;
            if destroyed == 200 {
                println!("#{} {}x{}", destroyed, x, y);
                break 'outer;
            }
            map.set(x, y, false);
        }
    }
}

const INPUT: &str = r#"
#..#.#.#.######..#.#...##
##.#..#.#..##.#..######.#
.#.##.#..##..#.#.####.#..
.#..##.#.#..#.#...#...#.#
#...###.##.##..##...#..#.
##..#.#.#.###...#.##..#.#
###.###.#.##.##....#####.
.#####.#.#...#..#####..#.
.#.##...#.#...#####.##...
######.#..##.#..#.#.#....
###.##.#######....##.#..#
.####.##..#.##.#.#.##...#
##...##.######..##..#.###
...###...#..#...#.###..#.
.#####...##..#..#####.###
.#####..#.#######.###.##.
#...###.####.##.##.#.##.#
.#.#.#.#.#.##.#..#.#..###
##.#.####.###....###..##.
#..##.#....#..#..#.#..#.#
##..#..#...#..##..####..#
....#.....##..#.##.#...##
.##..#.#..##..##.#..##..#
.##..#####....#####.#.#.#
#..#..#..##...#..#.#.#.##
"#;
