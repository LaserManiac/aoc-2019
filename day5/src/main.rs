enum Mode {
    Position,
    Immediate,
}

impl Mode {
    fn load(&self, pc: usize, state: &mut [i32]) -> i32 {
        match self {
            Mode::Position => state[state[pc] as usize],
            Mode::Immediate => state[pc],
        }
    }
}

enum Opcode {
    Add,
    Mul,
    In,
    Out,
    Jit,
    Jif,
    Lt,
    Eq,
    Halt,
}

struct Instr {
    opcode: Opcode,
    mode_1: Mode,
    mode_2: Mode,
    mode_3: Mode,
}

impl Instr {
    fn from_i32(val: i32) -> Instr {
        let opcode = match val % 100 {
            1 => Opcode::Add,
            2 => Opcode::Mul,
            3 => Opcode::In,
            4 => Opcode::Out,
            5 => Opcode::Jit,
            6 => Opcode::Jif,
            7 => Opcode::Lt,
            8 => Opcode::Eq,
            99 => Opcode::Halt,
            opc => panic!("Invalid opcode {}!", opc),
        };
        let mode_1 = if val / 100 % 10 == 0 { Mode::Position } else { Mode::Immediate };
        let mode_2 = if val / 1000 % 10 == 0 { Mode::Position } else { Mode::Immediate };
        let mode_3 = if val / 10000 % 10 == 0 { Mode::Position } else { Mode::Immediate };
        Self { opcode, mode_1, mode_2, mode_3 }
    }

    fn exec(&self, pc: usize, state: &mut [i32]) -> Option<usize> {
        match self.opcode {
            Opcode::Add => {
                let a = self.mode_1.load(pc + 1, state);
                let b = self.mode_2.load(pc + 2, state);
                let addr = state[pc + 3] as usize;
                state[addr] = a + b;
                Some(pc + 4)
            }
            Opcode::Mul => {
                let a = self.mode_1.load(pc + 1, state);
                let b = self.mode_2.load(pc + 2, state);
                let addr = state[pc + 3] as usize;
                state[addr] = a * b;
                Some(pc + 4)
            }
            Opcode::In => {
                let val = read_input();
                let addr = state[pc + 1] as usize;
                state[addr] = val;
                Some(pc + 2)
            }
            Opcode::Jit => {
                let x = self.mode_1.load(pc + 1, state);
                if x != 0 {
                    let pc = self.mode_2.load(pc + 2, state) as usize;
                    Some(pc)
                } else {
                    Some(pc + 3)
                }
            }
            Opcode::Jif => {
                let x = self.mode_1.load(pc + 1, state);
                if x == 0 {
                    let pc = self.mode_2.load(pc + 2, state) as usize;
                    Some(pc)
                } else {
                    Some(pc + 3)
                }
            }
            Opcode::Lt => {
                let a = self.mode_1.load(pc + 1, state);
                let b = self.mode_2.load(pc + 2, state);
                let addr = state[pc + 3] as usize;
                state[addr] = if a < b { 1 } else { 0 };
                Some(pc + 4)
            }
            Opcode::Eq => {
                let a = self.mode_1.load(pc + 1, state);
                let b = self.mode_2.load(pc + 2, state);
                let addr = state[pc + 3] as usize;
                state[addr] = if a == b { 1 } else { 0 };
                Some(pc + 4)
            }
            Opcode::Out => {
                let val = self.mode_1.load(pc + 1, state);
                println!("{}", val);
                Some(pc + 2)
            }
            Opcode::Halt => None,
        }
    }
}

fn read_input() -> i32 {
    let mut buffer = String::new();
    let _ = std::io::stdin().read_line(&mut buffer);
    buffer.trim().parse::<i32>().expect("Invalid i32!")
}

fn get_program() -> Vec<i32> {
    INPUT.split(',')
        .map(str::trim)
        .map(str::parse::<i32>)
        .filter_map(Result::ok)
        .collect()
}

fn main() {
    let mut program = get_program();
    let mut pc = 0;
    loop {
        let instr = Instr::from_i32(program[pc]);
        match instr.exec(pc, &mut program) {
            Some(next_pc) => pc = next_pc,
            None => break,
        }
    }
}

const INPUT: &str = "3,225,1,225,6,6,1100,1,238,225,104,0,1101,37,61,225,101,34,121,224,1001,224,-49,224,4,224,102,8,223,223,1001,224,6,224,1,224,223,223,1101,67,29,225,1,14,65,224,101,-124,224,224,4,224,1002,223,8,223,101,5,224,224,1,224,223,223,1102,63,20,225,1102,27,15,225,1102,18,79,224,101,-1422,224,224,4,224,102,8,223,223,1001,224,1,224,1,223,224,223,1102,20,44,225,1001,69,5,224,101,-32,224,224,4,224,1002,223,8,223,101,1,224,224,1,223,224,223,1102,15,10,225,1101,6,70,225,102,86,40,224,101,-2494,224,224,4,224,1002,223,8,223,101,6,224,224,1,223,224,223,1102,25,15,225,1101,40,67,224,1001,224,-107,224,4,224,102,8,223,223,101,1,224,224,1,223,224,223,2,126,95,224,101,-1400,224,224,4,224,1002,223,8,223,1001,224,3,224,1,223,224,223,1002,151,84,224,101,-2100,224,224,4,224,102,8,223,223,101,6,224,224,1,224,223,223,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,108,677,677,224,1002,223,2,223,1006,224,329,101,1,223,223,1107,677,226,224,102,2,223,223,1006,224,344,101,1,223,223,8,677,677,224,1002,223,2,223,1006,224,359,101,1,223,223,1008,677,677,224,1002,223,2,223,1006,224,374,101,1,223,223,7,226,677,224,1002,223,2,223,1006,224,389,1001,223,1,223,1007,677,677,224,1002,223,2,223,1006,224,404,1001,223,1,223,7,677,677,224,1002,223,2,223,1006,224,419,1001,223,1,223,1008,677,226,224,1002,223,2,223,1005,224,434,1001,223,1,223,1107,226,677,224,102,2,223,223,1005,224,449,1001,223,1,223,1008,226,226,224,1002,223,2,223,1006,224,464,1001,223,1,223,1108,677,677,224,102,2,223,223,1006,224,479,101,1,223,223,1108,226,677,224,1002,223,2,223,1006,224,494,1001,223,1,223,107,226,226,224,1002,223,2,223,1006,224,509,1001,223,1,223,8,226,677,224,102,2,223,223,1006,224,524,1001,223,1,223,1007,226,226,224,1002,223,2,223,1006,224,539,1001,223,1,223,107,677,677,224,1002,223,2,223,1006,224,554,1001,223,1,223,1107,226,226,224,102,2,223,223,1005,224,569,101,1,223,223,1108,677,226,224,1002,223,2,223,1006,224,584,1001,223,1,223,1007,677,226,224,1002,223,2,223,1005,224,599,101,1,223,223,107,226,677,224,102,2,223,223,1005,224,614,1001,223,1,223,108,226,226,224,1002,223,2,223,1005,224,629,101,1,223,223,7,677,226,224,102,2,223,223,1005,224,644,101,1,223,223,8,677,226,224,102,2,223,223,1006,224,659,1001,223,1,223,108,677,226,224,102,2,223,223,1005,224,674,1001,223,1,223,4,223,99,226";
